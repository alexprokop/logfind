﻿/*
 * Copyright © 2019,2020 Alexander Prokop
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See the
 * included COPYING file or http://www.wtfpl.net/ for more details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "logfind.h"

int eof = 0;

int getfield(FILE* file, int buflen, char* buf) {
	int len = 0;
	int quot = 0;
	char c = 0;

	while (len < buflen) {
		switch (c = getc(file)) {
		case '"':
			if (quot) {
				if ((c = getc(file)) == '"') {
					buf[len] = c;
					len++;
					break;
				} else {
					quot = !quot;
					ungetc(c, file);
					break;
				}
			} else {
				quot = !quot;
				break;
			}
			quot = !quot;
			break;
		case EOF:
			quot = 0;
			eof = 1;
			/* Fallthrough */
		case '\n':
			/* Fallthrough */
		case ',':
			if (!quot) {
				buf[len] = '\0';
				len++;
				return len;
			}
			/* Else fallthrough */
		default:
			buf[len] = c;
			len++;
			break;
		}
	}

	/* 
	 * If we run out of buffer, return -1.
	 * This works because caller specifies buflen anyway.
	 */
	return -1;
}

char getlevel(FILE* file) {
	char* buf;
	char c;
	int len;

	do {
		buf = malloc(16 * sizeof(char));
	} while (buf == NULL);
	len = getfield(file, 16, buf);
	
	c = buf[0];
	free(buf);

	return c;
}

char* getdate(FILE* file) {
	char* buf = NULL;

	buf = malloc(MAXDATE * sizeof(char));
	getfield(file, MAXDATE, buf);

	return buf;
}

char* getsrc(FILE* file) {
	int len;
	char* buf;

	buf = malloc(MAXSRC * sizeof(char));
	len = getfield(file, MAXSRC, buf);

	return buf;
}

int geteid(FILE* file) {
	char* buf;
	int len, ret;

	do {
		buf = malloc(MAXEID * sizeof(char));
	} while (buf == NULL);
	len = getfield(file, MAXEID, buf);

	ret = atoi(buf);
	free(buf);
	return ret;
}

int gettcat(FILE* file) {
	char* buf;
	int len;
	int tcat;

	do {
		buf = malloc(35 * sizeof(char));
	} while (buf == NULL);
	len = getfield(file, 35, buf);

	if (strncmp(buf, "None", 4) == 0) {
		free(buf);
		return -1;
	} else {
		sscanf_s(buf, "(%i)", &tcat);
		free(buf);
		return tcat;
	}
}

int getdesc(FILE* file, char* buf, int buflen) {
	return getfield(file, buflen, buf);
}

/* This is ugly, but still better than dealing with the intricacies of time... */
int strtodate(char* buf, struct date* d)
{
	sscanf_s(buf, "%i. %i. %i %i:%i:%i", \
		&d->day, &d->mon, &d->year, &d->hour, &d->min, &d->sec);

	return 0;
}

int datecmp(struct date* d1, struct date* d2)
{
	int ret;

	if ((ret = d1->year - d2->year) != 0)
		return ret;
	if ((ret = d1->mon - d2->mon) != 0)
		return ret;
	if ((ret = d1->day - d2->day) != 0)
		return ret;
	if ((ret = d1->hour - d2->hour) != 0)
		return ret;
	if ((ret = d1->min - d2->min) != 0)
		return ret;
	if ((ret = d1->sec - d2->sec) != 0)
		return ret;
	return 0;
}