/*
 * Copyright � 2019,2020 Alexander Prokop
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See the
 * included COPYING file or http://www.wtfpl.net/ for more details.
 */

#include <stdio.h>
#include <stdlib.h>
#include "boymo.h"

/* Bad Character table */
int* d1_init(char* pat, int patlen)
{
	int* d1;
	int i;

	while ((d1 = malloc(MAXALPHA * sizeof(int))) == NULL);

	for (i = 0; i < MAXALPHA; i++) {
		d1[i] = patlen;
	}

	for (i = 0; i < (patlen - 1); i++) {
		d1[pat[i]] = patlen - 1 - i;
	}

	return d1;
}

/* Good Suffix table */
int* d2_init(char* pat, int patlen)
{
	int* d2;
	int i;
	int j;

	/* 
	 * The (long long) cast is due to Visual Studio complaining due to 
	 * (int) patlen - 1 potentially underflowing in the implicit cast.
	 * This will never happen ;) for sane values of patlen.
	 */
	while ((d2 = malloc(((long long) patlen - 1) * sizeof(int))) == NULL);

	/* For each suffix */
	for (i = patlen - 2; i >= 0; i--) {
		/* For each position from the end of pattern */
		for (j = patlen - 2; j >= 0; j--) {
			if (is_suffix(pat, patlen, patlen - i - 1, j)) {
				break;
			}
		}
		d2[i] = patlen - 1 - j;
	}

	return d2;
}

int is_suffix(char* pat, int patlen, int sufflen, int pos) {
	int i;
	int match = 1;

	for (i = pos; (i >= 0) && (i >= (pos - sufflen)); i--) {
		if (i > (pos - sufflen)) {
			if (pat[i] != pat[patlen - 1 - (pos - i)]) {
				match = 0;
				break;
			}
		} else {
			if (pat[i] == pat[patlen - 1 - (pos - i)]) {
				match = 0;
				break;
			}
		}
	}

	return match;
}

int bm_search(char* pat, int patlen, char* text, int textlen, int* d1, int* d2)
{
	int i_pat = patlen - 1;
	int i_txt = i_pat;
	int d_pat;
	int s1;
	int s2;
	int match = -1;

	while (i_txt < textlen) {
		s1 = 0;
		s2 = 0;
		d_pat = patlen - 1 - i_pat;

		if (pat[i_pat] == text[i_txt - d_pat]) {
			if (i_pat == 0) {
				match = i_txt - (patlen - 1);
				break;
			} else {
				i_pat--;
			}
		} else {
			/* 
			 * Cast to unsigned char because table indexes
			 * would be negative for chars with 8th bit set.
			 */
			s1 = d1[(unsigned char) text[i_txt - d_pat]];
			if (s1 - d_pat < 0) {
				s1 = 1;
			} else {
				s1 -= d_pat;
			}

			if (i_pat == (patlen - 1)) {
				s2 = 0;
			} else {
				s2 = d2[i_pat];
			}
			i_txt += ((s1 < s2) ? s2 : s1);
			i_pat = patlen - 1;
		}
	}

	return match;
}