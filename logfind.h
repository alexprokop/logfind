/*
 * Copyright � 2019,2020 Alexander Prokop
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See the
 * included COPYING file or http://www.wtfpl.net/ for more details.
 */

#pragma once

/*
 * ------------------------------------------------------------
 * MACROS
 * ------------------------------------------------------------
 */
#define MAXDATE 32
#define MAXDESC 16384
#define MAXEID 64
#define MAXSRC 128

/*
 * ------------------------------------------------------------
 * STRUCTURES
 * ------------------------------------------------------------
 */
struct date {
	int day;
	int mon;
	int year;
	int hour;
	int min;
	int sec;
};

/*
 * ------------------------------------------------------------
 * EXTERNAL VARIABLES
 * ------------------------------------------------------------
 */
extern int eof;

/*
 * ------------------------------------------------------------
 * FUNCTIONS
 * ------------------------------------------------------------
 */
char* getdate(FILE*);
int getdesc(FILE*, char*, int);
int geteid(FILE*);
int getfield(FILE*, int, char*);
char getlevel(FILE*);
char* getsrc(FILE*);
int gettcat(FILE*);
int strtodate(char*, struct date*);
int datecmp(struct date*, struct date*);