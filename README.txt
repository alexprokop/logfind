L O G F I N D
=============
Parse and filter Windows log files exported to CSV.
Binaries for the AMD64 architecture are available in the bin/ directory.

Usage: logfind [-a] -[B|I] [-d date_min] [-D date_max] [-e eid] [-l level] 
		[-m pattern] [-o out_file] [-s source] [-t task_cat] [file]

	-a
		Append to the end of output file instead of overwriting it.

	-B
		Batch mode - avoids printing the interactive menu. If not 
		specified, the default is interactive mode (see -I).

	-d date_min -D date_max
		Filter log messages by a range of dates. 
		Format is "D. M. YYYY HH:MM:SS"

	-e eid
		Filter log messages by their Event ID.

	-I
		Interactive mode - prints an interactive menu to specify the
		setup. However, command line arguments are still processed and
		used as default values for the menu. This is the default.

	-l level
		Filter by Log Level. "Level" is a single character: I, W, or E.

	-m pattern
		Filter by matching "pattern" in the log message description.
		"Pattern" is searched for verbatim - there are no 
		meta-characters.

	-o out_file
		Output file. If it already exits, it WILL be overwritten unless 
		"-a" is specified as well. If not specified, standard output is 
		used.

	-s source
		Filter log messages according to their Source.

	-t task_cat
		Filter log messages according to their task category.

	file
		Input file. If not specified, standard input is used.
