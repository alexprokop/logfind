/*
 * Copyright � 2019,2020 Alexander Prokop
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See the
 * included COPYING file or http://www.wtfpl.net/ for more details.
 */

#pragma once

/*
 * ------------------------------------------------------------
 * MACROS
 * ------------------------------------------------------------
 */
#define MAXALPHA 256

/*
 * ------------------------------------------------------------
 * FUNCTIONS
 * ------------------------------------------------------------
 */
int* d1_init(char*, int);
int* d2_init(char*, int);
int is_suffix(char*, int, int, int);
int bm_search(char*, int, char*, int, int*, int*);