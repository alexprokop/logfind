/*
 * Copyright � 2019,2020 Alexander Prokop
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See the
 * included COPYING file or http://www.wtfpl.net/ for more details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "logfind.h"
#include "boymo.h"

#define MAXFNAME 256

int main(int argc, char** argv) {
	int eid, tcat, i;
	struct {
		unsigned int append : 1;
		unsigned int bom : 1;
		unsigned int date : 1;
		unsigned int desc : 1;
		unsigned int eid : 1;
		unsigned int level : 1;
		unsigned int menu : 1;
		unsigned int print : 1;
		unsigned int src : 1;
		unsigned int tcat : 1;
	} flags;
	flags.append = 0;
	flags.bom = 0;
	flags.date = 0;
	flags.desc = 0;
	flags.eid = 0;
	flags.level = 0;
	flags.menu = 1;
	flags.print = 0;
	flags.src = 0;
	flags.tcat = 0;
	int m_eid = 0;
	int m_tcat = 0;
	int menusel = 0;
	int desc_l = 0;
	int m_desc_l = 0;
	int* d1 = NULL;
	int* d2 = NULL;
	char level = 0;
	char m_level = 0;
	char* src = NULL;
	char* desc = NULL;
	char* date = NULL;
	char* m_desc;
	char* m_src;
	char* m_date_max;
	char* m_date_min;
	char* fname;
	char* ofname;
	FILE* file = NULL;
	FILE* ofile = NULL;
	struct date* s_date = NULL;
	struct date* s_date_max = NULL;
	struct date* s_date_min = NULL;

	while ((m_desc = calloc(MAXDESC, sizeof(char))) == NULL);
	while ((m_src = calloc(MAXSRC, sizeof(char))) == NULL);
	while ((m_date_max = calloc(MAXDATE, sizeof(char))) == NULL);
	while ((m_date_min = calloc(MAXDATE, sizeof(char))) == NULL);
	while ((fname = calloc(MAXFNAME, sizeof(char))) == NULL);
	while ((ofname = calloc(MAXFNAME, sizeof(char))) == NULL);
	while ((s_date = malloc(sizeof(struct date))) == NULL);
	while ((desc = calloc(MAXDESC, sizeof(char))) == NULL);

	/* Handle arguments */
	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'a':
				flags.append = 1;
				break;
			case 'o':
				strncpy_s(ofname, MAXFNAME, argv[++i], MAXFNAME - 1);
				ofname[255] = '\0';
				break;
			case 'l':
				flags.level = 1;
				m_level = argv[++i][0];
				break;
			case 's':
				flags.src = 1;
				strncpy_s(m_src, MAXSRC, argv[++i], MAXSRC - 1);
				m_src[MAXSRC - 1] = '\0';
				break;
			case 'e':
				flags.eid = 1;
				m_eid = atoi(argv[++i]);
				break;
			case 't':
				flags.tcat = 1;
				m_tcat = atoi(argv[++i]);
				break;
			case 'm':
				flags.desc = 1;
				strncpy_s(m_desc, MAXDESC, argv[++i], MAXDESC - 1);
				m_desc[MAXDESC - 1] = '\0';
				break;
			case 'd':
				flags.date = 1;
				strncpy_s(m_date_min, MAXDATE, argv[++i], MAXDATE - 1);
				while (s_date_min == NULL)
					s_date_min = malloc(sizeof(struct date));
				(void) strtodate(m_date_min, s_date_min);
				break;
			case 'D':
				flags.date = 1;
				strncpy_s(m_date_max, MAXDATE, argv[++i], MAXDATE - 1);
				while (s_date_max == NULL)
					s_date_max = malloc(sizeof(struct date));
				(void) strtodate(m_date_max, s_date_max);
				break;
			case 'B':
				flags.menu = 0;
				break;
			case 'I':
				flags.menu = 1;
				break;
			}
		} else {
			strncpy_s(fname, MAXFNAME, argv[i], MAXFNAME - 1);
			fname[255] = '\0';
		}
	}

	/* Interactive menu */
	while (flags.menu) {
		printf("\n============================================================\n");
		printf("L O G F I N D\n\n");
		printf("--- Input and Output ---\n");
		printf("1 Input File: %s\n", fname);
		printf("2 Output File: %s\n", ofname);
		printf("--- Filters ---\n");
		printf("3 Level: %c\n", m_level);
		printf("4 Source: %s\n", m_src);
		printf("5 Event ID: %i\n", m_eid);
		printf("6 Task Category: %i\n", m_tcat);
		printf("7 Date Range: %s - %s\n", m_date_min, m_date_max);
		printf("8 Description: %s\n", m_desc);
		printf("--- Action ---\n");
		printf("9 Go!\n");
		printf("0 Exit\n");
		printf("---\n");
		printf("=> ");

		if (scanf_s("%i", &menusel) < 1) {
			printf("ERROR: Wrong input!\n");
			continue;
		}
		
		/* Dispose of the leftover newline */
		(void) getchar();

		switch (menusel) {
		case 1:
			printf("Specify Input File: ");
			gets_s(fname, MAXFNAME);
			break;
		case 2:
			printf("Specify Output File: ");
			gets_s(ofname, MAXFNAME);
			break;
		case 3:
			printf("Specify Log Level (I, W, E): ");
			m_level = getchar();
			flags.level = 1;
			break;
		case 4:
			printf("Specify Source: ");
			gets_s(m_src, MAXSRC);
			flags.src = 1;
			break;
		case 5:
			printf("Specify Event ID: ");
			scanf_s("%i", &m_eid);
			flags.eid = 1;
			break;
		case 6:
			printf("Specify Task Category: ");
			scanf_s("%i", &m_tcat);
			flags.tcat = 1;
			break;
		case 7:
			printf("Specify Start Date (D. M. YYYY HH:MM:SS): ");
			gets_s(m_date_min, MAXDATE);
			while (s_date_min == NULL)
				s_date_min = malloc(sizeof(struct date));
			(void)strtodate(m_date_min, s_date_min);
			printf("Specify End Date (D. M. YYYY HH:MM:SS): ");
			gets_s(m_date_max, MAXDATE);
			while (s_date_max == NULL)
				s_date_max = malloc(sizeof(struct date));
			(void)strtodate(m_date_max, s_date_max);
			flags.date = 1;
			break;
		case 8:
			printf("Specify Description: ");
			gets_s(m_desc, MAXDESC);
			flags.desc = 1;
			break;
		case 9:
			flags.menu = 0;
			break;
		case 0:
			printf("Exiting...\n");
			return 0;
			break;
		default:
			printf("ERROR: Wrong input!\n");
			continue;
		}
	}

	if (fname[0] == '\0')
		file = stdin;
	else
		fopen_s(&file, fname, "r");

	if (ofname[0] == '\0') {
		ofile = stdout;
	} else {
		fopen_s(&ofile, ofname, flags.append ? "a" : "w");
	}

	if (flags.desc) {
		m_desc_l = strlen(m_desc);
		d1 = d1_init(m_desc, m_desc_l);
		d2 = d2_init(m_desc, m_desc_l);
	}

	/* Read through the useless UTF-8 BOM and first line */
	while (getc(file) != '\n');

	do {
		/* If no match flags are set, print all by default */
		if (flags.level || flags.src || flags.eid || flags.tcat \
			|| flags.date || flags.desc)
			flags.print = 0;
		else
			flags.print = 1;

		level = getlevel(file);
		date = getdate(file);
		src = getsrc(file);
		eid = geteid(file);
		tcat = gettcat(file);
		desc_l = getdesc(file, desc, MAXDESC);

		if (desc_l == -1) {
			printf("WARNING: Ran out of buffer on description!");
			goto cleanup;
		}

		if (flags.level) {
			if (level == m_level) {
				flags.print = 1;
			} else {
				goto cleanup;
			}
		}

		if (flags.src) {
			if (strncmp(src, m_src, MAXSRC) == 0) {
				flags.print = 1;
			} else {
				goto cleanup;
			}
		}

		if (flags.eid) {
			if (eid == m_eid) {
				flags.print = 1;
			} else {
				goto cleanup;
			}
		}

		if (flags.tcat) {
			if (tcat == m_tcat) {
				flags.print = 1;
			} else {
				goto cleanup;
			}
		}

		if (flags.date) {
			(void)strtodate(date, s_date);
			if (s_date_min != NULL) {
				if (datecmp(s_date, s_date_min) >= 0) {
					flags.print = 1;
				} else {
					goto cleanup;
				}
			}
			if (s_date_max != NULL) {
				if (datecmp(s_date, s_date_max) <= 0) {
					flags.print = 1;
				} else {
					goto cleanup;
				}
			}
		}

		if (flags.desc) {
			if (bm_search(m_desc, m_desc_l, desc, desc_l, d1, d2) != -1) {
				flags.print = 1;
			} else {
				goto cleanup;
			}
		}

		if (flags.print && !eof) {
			fprintf(ofile, "LEVEL: %c\nDATE: %s\nSOURCE: %s\nEVENT: %i\nTASK: %i\nDESCRIPTION:\n%s\n", \
				level, date, src, eid, tcat, desc);
			fprintf(ofile, "============================================================\n");
		}

	cleanup:
		free(src);
		free(date);
	} while (!eof);

	fclose(file);
	fclose(ofile);

	return 0;
}